import Vue from 'vue'
import Router from 'vue-router'
import Home from '../components/Home'
import Login from '../components/Login'
import MyProfile from '../components/MyProfile'
import MyCards from '../components/MyCards'
import Save from '../components/Save'
import Withdraw from '../components/Withdraw'
import Purchase from '../components/Purchase'
import Settle from '../components/Settle'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Login',
      component: Login
    },
    {
      path: '/home',
      name: 'Home',
      component: Home
    },
    {
      path: '/mycards',
      name: 'MyCards',
      component: MyCards
    },
    {
      path: '/profile',
      name: 'Profile',
      component: MyProfile
    },
    {
      path: '/save',
      name: 'Save',
      component: Save
    },
    {
      path: '/withdraw',
      name: 'Withdraw',
      component: Withdraw
    },
    {
      path: '/purchase',
      name: 'Purchase',
      component: Purchase
    },
    {
      path: '/settle',
      name: 'Settle',
      component: Settle
    }
  ]
})
