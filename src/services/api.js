import axios from 'axios'

export default() => {
  return axios.create({
    baseURL: 'https://bank-system-api-prod.herokuapp.com/'
  })
}
