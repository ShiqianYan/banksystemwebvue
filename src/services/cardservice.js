import Api from '../services/api'

export default {
  postCard (card) {
    return Api().post('/card', card,
      {headers: {'Content-type': 'application/json'}})
  },
  deleteCard (id) {
    return Api().delete(`/card/${id}`)
  },
  fetchCard (userName) {
    return Api().get(`/cardVUN/${userName}`)
  },
  saveMoney (id, operation) {
    return Api().put(`/card/save/${id}`, operation,
      {headers: {'Content-type': 'application/json'}})
  },
  withdrawMoney (id, operation) {
    return Api().put(`card/withdraw/${id}`, operation,
      {headers: {'Content-type': 'application/json'}})
  },
  purchaseExchange (id, operation) {
    return Api().put(`card/purchase/${id}`, operation,
      {headers: {'Content-type': 'application/json'}})
  },
  settleExchange (id, operation) {
    return Api().put(`card/settle/${id}`, operation,
      {headers: {'Content-type': 'application/json'}})
  }
}
