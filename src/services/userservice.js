import Api from '../services/api'

export default {
  postUser (user) {
    return Api().post('/user', user,
      {headers: {'Content-type': 'application/json'}})
  },
  findUserPasswordViaUserName (userName) {
    return Api().get(`/userVUN/${userName}`)
  }
}
