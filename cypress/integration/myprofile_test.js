describe('My profile test', () =>{
  beforeEach(() => {
    cy.visit('http://localhost:8080/#/profile')
  })
  it('should display my profile interface', function () {
    cy.get('div').should('have.class', 'hero')
    cy.get('i').should('have.class', 'fas fa-user')
    cy.get('i').should('have.class', 'fas fa-credit-card')
    cy.get('i').should('have.class', 'fas fa-phone-volume')
    cy.get('h3').should('contain', 'My card')
    cy.get('h3').should('contain', 'My phoneNumber')
    cy.get('#cardNumber').click()
    cy.url().should('contain', 'http://localhost:8080/#/mycards')
  })
})
