describe('Home test', () => {
  beforeEach(() => {
    cy.visit('http://localhost:8080/#/home')
  })
  it('should display text', function () {
    cy.get('div').should('have.class', 'hero')
    cy.get('#homeH1').should('have.class', 'vue-title')
    cy.get('#homeH1').should('contain', 'Homer for President')
    cy.get('#homeH3').should('contain', 'Time for saving money!!!')
  })
})
