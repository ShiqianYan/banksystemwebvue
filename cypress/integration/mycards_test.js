describe('MyCards test', () => {
  beforeEach(() => {
    cy.visit('http://localhost:8080/#/mycards')
  })
  it('should diplay my cards interface', function () {
    cy.get('div').should('have.class', 'hero')
    cy.get('h3').should('have.class', 'vue-title')
    cy.get('i').should('have.class', 'far fa-credit-card')
    cy.get('img').should('have.class', 'img')
    cy.get('h3').should('have.class', 'cardInfo')
    cy.get('i').should('contain', 'Card Number')
  })
})
