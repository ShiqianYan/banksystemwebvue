describe('Purchase test', () => {
  beforeEach(() => {
    cy.visit('http://localhost:8080/#/purchase')
  })
  it('should display purchase interface', function () {
    cy.get('div').should('have.class', 'hero')
    cy.get('h3').should('have.class', 'vue-title')
    cy.get('i').should('have.class', 'fa fa-money')
    cy.get('div').should('have.class', 'row justify-content-center')
    cy.get('div').should('have.class', 'col-md-6')
    cy.get('div').should('have.class', 'form-group')
    cy.get('#aLabel').should('have.class', 'form-control-label')
    cy.get('#aLabel').should('contain', 'Amount (1-2000)')
    cy.get('#amount').should('have.class', 'form_input')
    cy.get('#amount').should('have.attr', 'type')
      .should('contain', 'number')
    cy.get('div').should('have.class', 'error')
    cy.get('div').should('contain', 'Amount is between 1 and 2000')
    cy.get('#pLabel').should('have.class', 'form-control-label')
    cy.get('#pLabel').should('contain', 'Password (6-digit)')
    cy.get('#password').should('have.class', 'form_input')
    cy.get('#password').should('have.attr', 'type')
      .should('contain', 'password')
    cy.get('div').should('have.class', 'error')
    cy.get('div').should('contain', 'Password is required')
    cy.get('#sub').should('have.class', 'btn btn-primary btn1')
    cy.get('#sub').should('have.attr', 'type')
      .should('contain', 'submit')
  })
})
